/**
 * Service Statistiques des demandes de travaux
 */
let stats_wr = function () {
    let new_stats = function () {
        return {
            "stats_wr_created": 0,
            "stats_wr_closed": 0,
            "stats_wr_removed": 0
        };
    };
    /**
     * fonction permettan de normaliser les réponses du service
     * @param data
     * @returns {{data: *, success: boolean}}
     */
    let object_return = function (data) {
        return {
            "success": true,
            "data": data
        }
    };

    /**
     * fonction permettant d'incrémenter les demandes de travaux créées
     * @param applicant le nom du demandeur
     */
    let inc_wr = function (applicant) {
        let obj = db_stat[applicant];
        if (obj === undefined || obj === null) {
            db_stat[applicant] = new_stats();
            db_stat[applicant].applicant = applicant;
        }
        db_stat[applicant].stats_wr_created += 1;
    };

    /**
     * fonction permettant d'incrémenter les demandes de travaux supprimées
     * @param applicant le nom du demandeur
     */
    let dec_wr = function (applicant) {
        let obj = db_stat[applicant];
        if (obj !== undefined || obj != null) {
            db_stat[applicant].stats_wr_removed += 1;
        }
    };

    /**
     * fonction permettant d'incrémenter les demandes de travaux closes
     * @param applicant le nom du demandeur
     */
    let close_wr = function (applicant) {
        let obj = db_stat[applicant];
        if (obj !== null) {
            db_stat[applicant].stats_wr_closed += 1;
        }
    };

    /**
     * fonction permettant de calculer le nombre de demandes de travaux ouvertes pour un demandeur
     * @param applicant le nom du demandeur
     * @returns {number} stats_wr_opened
     */
    let compute_wr_open = function (applicant) {
        let obj = db_stat[applicant];
        if (obj === undefined || obj === null) {
            return 0;
        }
        return obj.stats_wr_created - obj.stats_wr_removed - obj.stats_wr_closed;
    };

    let db_stat = {};

    /**
     * fonction permettant d'incrémenter les demandes de travaux créées (stats_wr_created) d'un demandeur
     */
    this.add('role:stats, st:creation, applicant:*', function (msg, respond) {
        let msg_err = "";
        if (msg.applicant === undefined || msg.applicant === null) {
            msg_err = "Error in applicant : is undefined";
        }

        // si erreur on ne fait rien
        if (msg_err !== "") {
            return;
        }
        // sinon on incrémente
        inc_wr(msg.applicant);
    });

    /**
     * fonction permettant d'incrémenter les demandes de travaux supprimées (stats_wr_removed) d'un demandeur
     */
    this.add('role:stats, st: decStats', function (msg, respond) {
        let msg_error = "";
        if (msg.applicant === undefined || msg.applicant === null) {
            msg_error = "Error in applicant : is undefined";
        }
        // si erreur on ne fait rien
        if (msg_error !== "") {
            return;
        }
        // sinon on incrémente
        dec_wr(msg.applicant);
    });

    /**
     * fonction permettant d'incrémenter les demandes de travaux closes (stats_wr_closed) d'un demandeur
     */
    this.add('role:stats, st:close', function (msg, respond) {
        let msg_error = "";
        if (msg.applicant === undefined || msg.applicant === null) {
            msg_error = "Error in applicant : is undefined";
        }
        // si erreur on ne fait rien
        if (msg_error !== "") {
            return;
        }
        // sinon on incrémente
        close_wr(msg.applicant);
    });

    /**
     *  fonction permettant de réaliser des statistiques générale des demandes de travaux
     *  ou bien celles d'un demandeur si précisé en paramètre du message
     */
    this.add('role:stats, st:get', function (msg, respond) {
        let data = msg.args.params;
        if (data.applicant === undefined || data.applicant === null) {
            let obj = {
                "applicant": data.applicant,
                "global_stats_wr_closed": 0,
                "global_stats_wr_removed": 0,
                "global_stats_wr_created": 0,
                "global_stats_wr_opened": 0,
            };

            // pour tous les demandeurs on récupère les valeurs des variables
            // et on les additionne pour avoir des statistiques globales
            for (let [applicant, value] of Object.entries(db_stat)) {
                obj.global_stats_wr_closed += value.stats_wr_closed;
                obj.global_stats_wr_removed += value.stats_wr_removed;
                obj.global_stats_wr_created += value.stats_wr_created;
                obj.global_stats_wr_opened += compute_wr_open(applicant);
            }
            respond(null, object_return(obj));
        } else {
            // sinon on retourne les statistiques du demandeur que l'on souhaite
            let obj = Object.assign({"stats_wr_opened": compute_wr_open(data.applicant)}, db_stat[data.applicant]);
            respond(null, object_return(obj))
        }
    });
};

/**
 * Serveur écoutant sur le port 4001 (communication avec la passerelle server.js et avec le service api.js)
 * @type {http.Server|*|*|*}
 */
let seneca = require("seneca")()
    .use(stats_wr)
    .quiet()
    .listen(4001);


