// importation de la bibliotheque
let minisearch = require("minisearch");
// definition des valeurs des clés des termes à indexer
let mask_idx = ["work", "applicant", "state"];
let options = {
    "fields": mask_idx
};

/**
 * Service Indexation
 */
let index_wr = function () {
    let engine = new minisearch(options);

    /**
     * fonction permettant d'indexer des termes
     */
    this.add("role:index, idx:suscribe", function (msg) {
        if (msg.object === undefined || msg.object === null) {
            return;
        }
        engine.add(msg.object);
    });

    /**
     * fonction permettant d'enlever l'indexation des termes
     */
    this.add("role:index, idx:unsuscribe", function (msg) {
        if (msg.object === undefined || msg.object === null) {
            return;
        }
        engine.remove(msg.object);
    });

    /**
     * fonction de recherche
     */
    this.add("role:index, idx:search", function (msg, response) {
        if (msg.search === undefined || msg.search === null) {
            response(null, null);
            return;
        }
        let searchItems = engine.search(msg.search);
        response(null, searchItems)
    })
};

/**
 * Serveur écoutant sur le port 4002 (communication avec le service api.js)
 * @type {http.Server|*|*|*}
 */
let seneca = require("seneca")()
    .use(index_wr)
    .quiet()
    .listen(4002);
