/**
 * Service CRUD des demandes de travaux
 */
let proceess_wr = function () {

    let dico = {};
    let count_id = 0;

    /**
     * fonction permettant de normaliser les réponses de types erreurs
     * @param msg_error
     * @returns {{msg: *, success: boolean}}
     */
    let object_error = function (msg_error) {
        return {
            "success": false,
            "msg": msg_error
        }
    };

    /**
     * fonction permettan de normaliser les réponses du service
     * @param data
     * @returns {{data: *, success: boolean}}
     */
    let object_return = function (data) {
        return {
            "success": true,
            "data": data
        }
    };

    /**
     * fonction permettant de transformer un objet en array
     * @param data
     * @returns {Array}
     */
    let dico_asArray = function (data) {
        let result = [];
        for (let [key, obj] of Object.entries(data)) {
            result.push(Object.assign({"id": key}, obj))
        }
        return result;
    };

    /**
     * fonction de création d'une demande de travaux
     * methode d'appel : POST
     */
    this.add('role:wr, cmd: create', function (msg, respond) {
        let data = msg.args.body;
        let object = Object.assign({"dt_wanted": undefined, "dt_done": undefined}, data);
        let msg_err = "";

        if (object.applicant === undefined || object.applicant === null) {
            msg_err = "Error in applicant : is undefined";
        } else if (object.work === undefined || object.work === null) {
            msg_err = "Error in work : is undefined";
        }

        if (msg_err !== "") {
            respond(null, object_error(msg_err));
            return;
        }
        count_id++;

        let obj = {
            "id": count_id,
            "applicant": object.applicant,
            "work": object.work,
            "dt_wanted": object.dt_wanted,
            "dt_done": object.dt_done,
            "state": "created",
        };
        dico[count_id] = obj;
        this.act("role:stats, st:creation", {applicant: object.applicant});
        this.act("role:index, idx:suscribe", {object: dico[count_id]});
        respond(null, object_return(Object.assign({id: count_id}, obj)));
    });


    /**
     * fonction permettant d'afficher une ou plusieurs demandes de travaux
     * methode d'appel : GET
     */
    this.add('role:wr, cmd: get', function (msg, respond) {
            let query = msg.args.query;
            let data = msg.args.params;
            let msg_error = "";
            let array_query = Object.entries(query);

            if (array_query.length > 1 || (array_query.length === 1 && array_query[0][0] !== "search")) {
                msg_error = "query parameter invalid";
            } else if (JSON.stringify(query) !== JSON.stringify({}) && data.id !== undefined) {
                msg_error = "query not possible with wr_id";
            } else if (data.id !== undefined && dico[data.id] === undefined) {
                msg_error = "wr_id is unknown";
            }

            if (msg_error !== "") {
                respond(null, object_error(msg_error));
                return;
            }

            // search by id
            if (data.id !== undefined) {
                respond(null, object_return(dico[data.id]));
            } else if (query.search === undefined && data.id === undefined) {
                respond(null, object_return(dico_asArray(dico)));
            } else {
                this.act("role:index, idx:search", {search: query.search}, function (err, response) {
                    let result = [];
                    response.forEach(function (item) {
                        result.push(dico[item.id])
                    });
                    respond(err, object_return(result))
                });
            }
        }
    );

    /**
     * fonction permettant de supprimer une ou plusieurs demandes de travaux
     * methode d'appel : DELETE
     */
    this.add('role:wr, cmd:delete', function (msg, respond) {
        let data = msg.args.params;
        let msg_error = "";

        if (data.id === undefined || data.id == null) {
            for (let [key, obj] of Object.entries(dico)) {
                if (obj.state !== 'closed') {
                    this.act("role:stats, st:decStats", {"applicant": obj.applicant});
                    this.act("role:index, idx:unsuscribe", {object: obj});
                    delete dico[key];
                }
            }

            respond(null, object_return({}));
            return;
        } else if (dico[data.id] === undefined) {
            msg_error = "wr not found";
        } else if (dico[data.id].state === "closed") {
            msg_error = "wr is already closed";
        }

        if (msg_error !== "") {
            respond(null, object_error(msg_error));
            return;
        }

        let object = dico[data.id];
        this.act("role:stats, st:decStats", {"applicant": dico[data.id].applicant});
        this.act("role:index, idx:unsuscribe", {object: object});
        delete dico[data.id];
        respond(null, object_return(object))
    });


    /**
     * fonction permettant de mettre à jour une demande de travaux
     * methode d'appel : UPDATE
     * si l'id n'est pas fournit : retourne "wr id not provided"
     * si l'id n'est pas dans le dico : retourne "wr not found"
     * si une demande est déjà à l'état closed : retourne "wr is already closed"
     */
    this.add('role:wr, cmd: update', function (msg, respond) {
        let data = msg.args.body;
        let params = msg.args.params;
        let msg_err = "";
        let id_to_search = params.id;

        if (params.id === undefined || params.id === null) {
            msg_err = "wr id not provided";
        } else if (dico[id_to_search] === undefined) {
            msg_err = "wr not found";
        } else if (dico[id_to_search].state === "closed") {
            msg_err = "wr is already closed";
        }
        // if state is different from closed
        if (msg_err === "") {
            let old = Object.assign({}, dico[id_to_search]);
            if (data.work !== undefined)
                dico[id_to_search].work = data.work;
            if (data.state !== undefined)
                dico[id_to_search].state = data.state;

            if (dico[id_to_search].state === "closed") {
                this.act("role:stats, st:close", {"applicant": dico[id_to_search].applicant});
            }

            this.act("role:index, idx:unsuscribe", {object: old});
            this.act("role:index, idx:suscribe", {object: dico[id_to_search]});
            respond(null, object_return(dico[id_to_search]));
            return;
        }
        respond(null, object_error(msg_err));
    });
};

/**
 * Serveur écoutant sur le port 4000 (communication avec la passerelle server.js)
 * Client pour la communication avec le module statistiques sur le port 4001
 * Client pour la communication avec le module indexation sur le port 4002
 * @type {http.Server|*|*|*}
 */
let seneca = require("seneca")()
    .quiet()
    .use(proceess_wr)
    .client({
        port: 4001,
        pin: 'st:*'
    })
    .client({
        port: 4002,
        pin: 'idx:*'
    })
    .listen(4000);