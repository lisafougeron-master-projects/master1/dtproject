const Seneca = require('seneca');
const SenecaWeb = require('seneca-web');
const Express = require('express');
const seneca = Seneca();
const BodyParser = require('body-parser');
seneca.quiet();


// definition des routes
let Routes = [
    {
        // route pour les stats
        prefix: '/api/wr/stats',
        pin: 'role:stats,st:*',
        map: {
            get: {GET: true, name: '', suffix: '/:applicant?'}
        }
    },
    {
        // route pour les works
        prefix: '/api/wr',
        pin: 'role:wr,cmd:*',
        map: {
            get: {GET: true, name: '', suffix: '/:id?'},
            update: {PUT: true, name: '', suffix: '/:id?'},
            create: {POST: true, name: '', suffix: '/'},
            delete: {DELETE: true, name: '', suffix: '/:id?'},
        }
    }];

seneca.use(SenecaWeb, {
    options: {parseBody: false}, // desactive l'analyseur JSON de Seneca
    routes: Routes,
    context: Express().use(BodyParser.json()),     // utilise le parser d'Express pour lire les donnees
    adapter: require('seneca-web-adapter-express') // des requetes PUT
});

// client pour le service wr
seneca.client({
    port: 4000,
    pin: 'cmd:*'
});

// client pour le service stat
seneca.client({
    port: 4001,
    pin: 'st:*'
});

// le serveur ecoute sur le port 3010
seneca.ready(() => {
    const app = seneca.export('web/context')();
    app.listen(3010)
});
