#!/bin/bash

const { exec } = require('child_process');

exec('node ./gestion_ms/stop_ms.js', (err, stdout, stderr) => {
    if (err) {
        console.error(`exec error: ${err}`);
        return;
    }

    if (stderr.length !== 0) {
        console.log(stderr);
    }

    exec('node ./gestion_ms/start_ms.js', (err, stdout, stderr) => {
        if (err) {
            console.error(`exec error: ${err}`);
            return;
        }

        console.log(stderr.length);
        if (stderr.length !== 0) {
            console.log(stderr);
        }
    });
});

