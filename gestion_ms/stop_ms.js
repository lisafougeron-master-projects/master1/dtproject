const exec = require('child_process').exec;

// to terminate the different started microservices...
exec('ps x', (error, stdout, stderr) => {
    if (error) {
        console.error(`exec error: ${error}`);
        return;
    }
    //console.log(`stdout\n: ${stdout}`);
    // find node processes from ps output
    var lines = stdout.split("\n");

    lines.forEach(function (line, index) {
        var items = line.split(" ");
        if (line.indexOf("?") >= 0 && line.indexOf("node") >= 0) {
            var num = line.split(" ").findIndex(function (element) {
               return element === "?";
            });
            var killcmd = 'kill -9 ' + items[num -1];
            exec(killcmd, (error, stdout, stderr) => {
                if (error) {
                    console.error(`exec error: ${error}`);
                    return;
                }
                if (stderr.length !== 0) {
                    console.log(`stderr: ${stderr}`);
                }
            })
        }
    });
});