'use strict';

const fs = require('fs');
const spawn = require('child_process').spawn;

// same logging for microservices...
const out = fs.openSync('./out.log', 'a');
const err = fs.openSync('./out.log', 'a');

// launch options
const spawn_opts = {
  detached: true,
  stdio: [ 'ignore', out, err ]
};

// launch microservices...
const app = spawn(process.argv[0], ['./main/server.js','--seneca.log.info'], spawn_opts);
app.unref();

const dt = spawn(process.argv[0], ['./main/api.js','--seneca.log.info'], spawn_opts);
dt.unref();


const stats = spawn(process.argv[0], ['./main/stats.js','--seneca.log.info'], spawn_opts);
stats.unref();

const index = spawn(process.argv[0], ['./main/index.js','--seneca.log.info'], spawn_opts);
index.unref();

console.log('test');