# Projet architecture orientée service - dt-project

### Auteurs
- Florian Bertonnier
- Lisa Fougeron

### Technologie
- Node
- Seneca

### Description
dt-project est un projet permettant de gérer des demandes de travaux. Il est composé de différents services tels que :
- le service de gestion des demandes
- le service statistique
- le service d'indexation

### Installation des dépendances
dt-project utilise node.js et la bibliothèque seneca. Pour mettre en place le projet, il faut mettre à jour les dépendances :
- executer la commande : `npm install`

### Lancement du projet
- executer le script start_ms.js : `node /gestion_ms/start_ms.js`
- stopper le script stop_ms.js : `node /gestion_ms/stop_ms.js`
